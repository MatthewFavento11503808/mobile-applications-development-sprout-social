package id11503808.sproutfinal;

import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.Date;

/**
 * Created by matt on 27/05/2015.
 */

@ParseClassName("Sprouts")
public class SproutPost extends ParseObject {

    public String getName() {
        return getString("name");
    }

    public void setName(String value) {
        put("name", value);
    }

    public String getVenue() {
        return getString("venue");
    }

    public void setVenue(String value) {
        put("venue", value);
    }

    public ParseUser getUser() {
        return getParseUser("user");
    }

    public void setUser(ParseUser value) {
        put("user", value);
    }

    public String getAction(){
        return getString("action");
    }

    public void setAction(String value){
        put("action", value);
    }

    public String getText() {
        String text = getAction() + " " + getVenue() + " at "+  getTime().toString();
        return text;
    }

    public Date getTime(){
        return (Date)get("location_date");
    }

    public void setTime(Date value){
        put("location_date", value);
    }

    public static ParseQuery<SproutPost> getQuery() {
        return ParseQuery.getQuery(SproutPost.class);
    }
}

package id11503808.sproutfinal;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseSession;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 *  Please view 'FragmentProfile' as they have similar methods and work in a similar way
 *
 */

public class FragmentFriendProfile extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    ParseUser mCurrentUser;
    String mFriendId;
    ParseUser mFriendUser;
    SproutUser mFriend;
    TextView mBadgeName;
    TextView mBadgeSproutNumber;
    private ListView mPostsLv;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ParseQueryAdapter mPostAdapter;
    ImageView mBadgePicture;
    private ParseQueryAdapter.QueryFactory<SproutPost> mFactory;
    Boolean isFriend = false;
    FriendRequest mFriendRequest;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mCurrentUser = ParseUser.getCurrentUser();
        mFriendId = args.getString("friendId");
        query();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_friend_profile, container, false);
        //query to find user via objectId
        mPostsLv = (ListView) view.findViewById(R.id.posts_lv);
        mBadgeName = (TextView) view.findViewById(R.id.badge_name);
        mBadgeSproutNumber = (TextView) view.findViewById(R.id.badge_sprouts);
        mBadgePicture = (ImageView) view.findViewById(R.id.badge_picture);

        setHasOptionsMenu(true);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        userPostsQueryFactory();
        mPostAdapter = new ParseQueryAdapter<SproutPost>(getActivity(), mFactory) {

            @Override
            public View getItemView(SproutPost sprout, View view, ViewGroup parent) {
                if (view == null) {
                    view = View.inflate(getActivity(), R.layout.posts_item, null);
                }
                //ImageView profilePic = (ImageView) view.findViewById(R.id.poster_image);
                TextView postName = (TextView) view.findViewById(R.id.post_name);
                TextView postText = (TextView) view.findViewById(R.id.post_text);
                postName.setText(sprout.getName());
                postText.setText(sprout.getText());

                return view;
            }




        };

        mPostAdapter.notifyDataSetChanged();

        mSwipeRefreshLayout.setOnRefreshListener(this);
        mPostsLv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (mPostsLv == null || mPostsLv.getChildCount() == 0) ?
                                0 : mPostsLv.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 &&
                        topRowVerticalPosition >= 0);
            }
        });
        mPostAdapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener() {
            @Override
            public void onLoading() {
                //set progress bar to visible
            }

            @Override
            public void onLoaded(List list, Exception e) {

            }
        });

        return view;
    }

    /**
     *  This query only ran after the query, hence the rame
     */


    private void postQueryExecution(){
        if(checkIfExistingFriend()) {
            if (mFriendId == mFriend.getmObjId()) {
                fillProfileWithFriendData();
                new GetProfilePicFromUrl(mFriend.getmProfileUrl()).execute();

            }
        }else {
            makeNewSproutUser(mFriendId);
            postQueryExecution();
        }
    }
        /**
         * Method not used as of yet
         */
//    private void createFriendRequest(){
//        mFriendRequest = new FriendRequest();
//        mFriendRequest.setSender(mCurrentUser.getObjectId());
//        mFriendRequest.setReceiver(mFriend.getmObjId());
//        mFriendRequest.saveInBackground(new SaveCallback() {
//            @Override
//            public void done(ParseException e) {
//            }
//        });
//
//    }

    private boolean checkIfExistingFriend() {
        if (mFriend != null) {
            //User exists
            //check if the same user as passed objectId
           return true;
        } else {
            return false;
        }
    }

    private void fillProfileWithFriendData() {
        mBadgeName.setText(mFriend.getmName());
        mBadgeSproutNumber.setText(mFriend.getmSproutNumber());
        Toast.makeText(getActivity(), "Setting adapter...", Toast.LENGTH_SHORT).show();
        mPostsLv.setAdapter(mPostAdapter);
        mPostAdapter.notifyDataSetChanged();

    }



    private void makeNewSproutUser(String friendId){
        mFriend = new SproutUser();
        mFriend.setmObjId(friendId);
        mFriend.setmFlist((ArrayList<String>) mFriendUser.get("fList"));
        mFriend.setmName(mFriendUser.getString("fb_name"));
        mFriend.setmProfileUrl(mFriendUser.getString("picture_url"));
    }

    /**
     * This method gets the friend user from the server via the passed 'mFriendId'
     */

    public void query() {
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("objectId", mFriendId);
        query.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser pUser, ParseException e) {
                if (e == null) {
                    mFriendUser = pUser;
                    postQueryExecution();

                } else {
                    Log.e(Constants.SPROUT_TAG, e.toString());
                }
            }
        });
    }

    private void userPostsQueryFactory(){
        mFactory = new ParseQueryAdapter.QueryFactory<SproutPost>() {
            public ParseQuery<SproutPost> create() {
                ParseQuery<SproutPost> query = SproutPost.getQuery();
                query.include("user");
                query.whereEqualTo("user", mFriendUser);
                query.setLimit(12);
                query.orderByDescending("createdAt");
                return query;
            }
        };
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_friend_profile, menu);
        if (isFriend()){
            Toast.makeText(getActivity(), "Already friends", Toast.LENGTH_SHORT).show();
            menu.getItem(1).setEnabled(true);
        }else if(mFriendRequest != null){
            Toast.makeText(getActivity(),"Already asked to be friends",Toast.LENGTH_SHORT).show();
            menu.getItem(2).setEnabled(true);
        }else{
            Toast.makeText(getActivity(),"Eligible for friendship",Toast.LENGTH_SHORT).show();
            menu.getItem(0).setEnabled(true);

        }
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.dev_action_post: //DEV SETTING
                //create friend request between mCurrentUser and mFriendUser
               // createPost();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onRefresh() {
        Toast.makeText(getActivity(), "Refreshing", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mPostAdapter.loadObjects();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 2000);
    }

    private boolean isFriend(){
        if (isFriend = true){
            return true;
        }else{
            return false;
        }

    }

    private boolean doesFriendRequestExist(){
        if (mFriendRequest != null){
            return true;
        }else{
            return false;
        }
    }



    public class GetProfilePicFromUrl extends AsyncTask<Void, Bitmap, Void> {

        String mImgUrl;

        public GetProfilePicFromUrl(String imgUrl) {
            super();
            this.mImgUrl = imgUrl;
            Log.i(Constants.SPROUT_TAG, "Getting image url " + mImgUrl);
            // do stuff
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(mImgUrl).getContent());
                publishProgress(bitmap);
            } catch (IOException e) {
                Log.i(Constants.SPROUT_TAG, e.toString());
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Bitmap... profPic) {
            super.onProgressUpdate(profPic);
            mBadgePicture.setImageBitmap(profPic[0]);

        }
    }
}



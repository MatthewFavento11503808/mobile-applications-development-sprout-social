package id11503808.sproutfinal;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * This is the main activity of my app, the bulk of my fragment methods are done from here*
 */

public class HomeActivity extends ActionBarActivity
{
    private FragmentDrawer mFDraw;
    private int mContainerRes;
    ParseUser mCurrentUser = ParseUser.getCurrentUser();
    private int PLACE_PICKER_REQUEST = 1;
    FragmentSprout mFragSprout;
    String mActionText;
    Date mLocationDate;
    LocationObject mLocation;
    boolean yesOrNo = false;
    String mSpinnerText;
    Place mPlace;
    SharedPreferences mSharedPreferences;


    /**
     * Temporary override of the back button so the user can't go back to the login screen and stuff with how the loginButton queries
     */

    @Override
    public void onBackPressed()
    {
        //No going back to login screen without logging out
    }

    /**
     * Sets up the drawer fragment and runs a 'setupDrawerConfiguration'
     * checks if the currentUser has a friends list
     * if they don't, create one
     * Default open view to user's profile
     */



    @Override
    protected void onResume() {
        super.onResume();
        if(checkPlayServices()){
            SharedPreferences sharedPref = getSharedPreferences(Constants.DEV_PREFS, Context.MODE_PRIVATE);
            boolean locationToggle = sharedPref.getBoolean("LocationServiceEnabled", false);
            String intervalTime = " " + sharedPref.getInt("IntervalAmt", 1);

            Log.d(Constants.SPROUT_TAG, "intervalTimeFromHome = " + intervalTime);
            Log.d(Constants.SPROUT_TAG, "Location services enabled = " + locationToggle);


        }else{
            Log.e(Constants.SPROUT_TAG, "Play services not enabled");
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        mFDraw = (FragmentDrawer) findViewById(R.id.drawer_layout);
        mContainerRes = R.id.frame_layout_content;
        mFDraw.setupDrawerConfiguration((ListView) findViewById(R.id.listViewNav), toolbar, mContainerRes);
        // Add nav items
        fillNavMenu();
        if (mCurrentUser != null){
            fillProfile();
            if (!hasFriends()){
                createFriendsList();
            }
        }
        mFDraw.selectDrawerItem(0);

    }

    /**
     *Creates a ArrayList<String> called friend list, adds Fav Cephei and myself to friends and saves it to Parse
     */

    private void createFriendsList(){
        Log.d(Constants.SPROUT_TAG, "Creating friends list...");
        ArrayList<String> friendsList = new ArrayList<>();
        friendsList.add(Constants.FAV_ID);
        friendsList.add(Constants.MATT_ID);
        mCurrentUser.put("fList", friendsList);
        mCurrentUser.saveInBackground();
    }



    /**
     * Returns whether or not user has a friends list
     */

    private boolean hasFriends() {
        if(mCurrentUser.has("fList")){
            return true;
        }else
            return false;
    }




    /**
     * Retrieves up to date profile information from facebook and stores it to ParseUser through a 'meRequest' that returns
     * a JSONObject from facebook that is the current user's facebook information.
     */

    public void fillProfile(){
        Log.d(Constants.SPROUT_TAG, "Getting facebook data....");
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

            @Override
            public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                if (jsonObject != null) {
                    Log.d(Constants.SPROUT_TAG, "Facebook data found!");
                    JSONObject profile = new JSONObject();
                    try {
                        profile.put("profile_picture_url", profilePictureString(jsonObject.getString("id")));
                        profile.put("facebookId", jsonObject.getString("id"));
                        profile.put("name", jsonObject.getString("name"));
                        mCurrentUser.put("fb_name", jsonObject.getString("name"));
                        mCurrentUser.put("picture_url", profilePictureString(jsonObject.getString("id")));
                        Log.i(Constants.SPROUT_TAG, profilePictureString(jsonObject.getString("id")));
                        if (jsonObject.getString("gender") != null) profile.put("gender", jsonObject.getString("gender"));
                        if (jsonObject.getString("email") != null) {
                            profile.put("email", jsonObject.getString("email"));
                            mCurrentUser.put("fb_email", jsonObject.getString("email"));
                        }
                        mCurrentUser.put("profile", profile);
                        mCurrentUser.saveInBackground();
                    } catch (JSONException e) {
                        Log.i(Constants.SPROUT_TAG, e.toString());
                    }
                }
            }
        });
        request.executeAsync();
    }

    /**
     * Returns profile picture string from a facebook Id
     */

    private String profilePictureString(String facebookId){
            return  "https://graph.facebook.com/" + facebookId + "/picture?type=large";
    }



    /**
     * Uses DrawerFragment methods to add them to the slider navigation drawer
     */

    private void fillNavMenu(){
        mFDraw.addNavItem("Home", "Your Profile", FragmentProfile.class, R.drawable.ic_home);
        mFDraw.addNavItem("Locations", "Your Locations", FragmentLocations.class, R.drawable.ic_locations);
        mFDraw.addNavItem("Matches", "Matches", FragmentMatches.class, R.drawable.ic_matches);
        mFDraw.addNavItem("Friends", "Your Friends", FragmentFriends.class, R.drawable.ic_friends);
        mFDraw.addNavItem("Settings", "Account Settings", FragmentSettings.class, R.drawable.ic_settings);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    /**
     * Method to swap to friends profile, called from FriendsFragment
     */

    public void switchToFriendProfile(Bundle args){
        FragmentFriendProfile fragFriend = new FragmentFriendProfile();
        fragFriend.setArguments(args);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(mContainerRes, fragFriend).commit();

    }

    /**
     * Method to open a Sprout DialogFragment, called from FragmentLocations
     */
    public void openSproutFragment(LocationObject location){
        //open FragmentSprout on top of the window, in the center.
        FragmentManager fragmentManager = getSupportFragmentManager();
        mFragSprout = new FragmentSprout();
        mFragSprout.setLocation(location);
        mFragSprout.show(fragmentManager, "Sprout");
    }




    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mFDraw.getDrawerToggle().syncState();
    }


    /**
     * Method to open PlacePicker API.
     *Here I set my own lat / lang bounds based on the locations latitiude and longitude
     */

    public void openLocationChooser(LocationObject location, String action){
            mActionText = action;
            mLocationDate = location.getCreatedAt();
            mLocation = location;

            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            LatLngBounds.Builder latLngBlder = new LatLngBounds.Builder();
            LatLng center = new LatLng(location.getLat(), location.getLongi());
            LatLng northEast = new LatLng(center.latitude + 0.001, center.longitude + 0.001);
            LatLng southWest = new LatLng(center.latitude - 0.001, center.longitude - 0.001);

            latLngBlder.include(northEast);
            latLngBlder.include(southWest);

            LatLngBounds latLngBounds = latLngBlder.build();
            builder.setLatLngBounds(latLngBounds);
            Context context = getApplicationContext();
            try {
                startActivityForResult(builder.build(context), PLACE_PICKER_REQUEST);
            } catch (GooglePlayServicesRepairableException e) {
                Log.i(Constants.SPROUT_TAG, e.toString());
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                Log.i(Constants.SPROUT_TAG, e.toString());
                e.printStackTrace();
            }

    }




    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mFDraw.getDrawerToggle().onConfigurationChanged(newConfig);
    }



    /**
     * Checks to see if the user has Play services enabled on their phone or not. They can't use the App if they don't
     */

    private boolean checkPlayServices() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(status)) {
                Log.e(Constants.SPROUT_TAG, "Status = " + status);
            } else {
                Toast.makeText(this, "This device is not supported.",
                        Toast.LENGTH_LONG).show();
            }
            return false;
        }
        return true;
    }

    /**
     * Returns a 'Place' object from PlacePicker API
     */

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                promptSproutDialog(mLocation, mSpinnerText, place);
            }else{
                Toast.makeText(this, "something went wrong, resultCode = " + resultCode, Toast.LENGTH_LONG).show();

            }
        }
    }

    /**
     * Converts place obtained from PlacePicker into a Sprout and save it to Parse
     */

    public void placeToSprout(Place place){
        SproutPost sproutPost = new SproutPost();
        sproutPost.setVenue((String) place.getName());
        sproutPost.setAction(mActionText);
        sproutPost.setUser(mCurrentUser);
        sproutPost.setName(mCurrentUser.getString("fb_name"));
        sproutPost.setTime(mLocationDate);

        sproutPost.saveEventually(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Toast.makeText(getApplicationContext(), "Sprouted!", Toast.LENGTH_LONG).show();
                deleteLocation(mLocation);
            }
        });


    }


    /**
     * Removes a location after the user deals with it (Sprouts or removes it)
     */
    public void deleteLocation(LocationObject locationObject){
        locationObject.getObjectId();
            ParseObject.createWithoutData("Locations", locationObject.getObjectId()).deleteEventually();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentLocations fr = (FragmentLocations) fragmentManager.findFragmentById(mContainerRes);
            fr.onRefresh();
            mFragSprout.dismiss();
            yesOrNo = false;


    }


    /**
     * Method to ask user whether they want to delete object or not
     */


    public void promptDeleteDialog(LocationObject location){
        mLocation = location;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirm");
        builder.setMessage(Constants.DELETE_LOCATION_MESSAGE);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                deleteLocation(mLocation);
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }


    /**
     * Method to ask user whether they want to sprout the object or not
     *
     */

    public void promptSproutDialog(LocationObject location, final String spinnerText, Place place){
        mLocation = location;
        mSpinnerText = spinnerText;
        mPlace = place;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirm");
        builder.setMessage(Constants.SPROUT_LOCATION_MESSAGE);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                placeToSprout(mPlace);
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
                dialog.dismiss();
            }});

        AlertDialog alert = builder.create();
        alert.show();

    }



    public void restartService(){
        stopService(new Intent(this, ServiceLocation.class));
        startService(new Intent(this, ServiceLocation.class));
    }

}

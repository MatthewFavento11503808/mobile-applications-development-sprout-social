package id11503808.sproutfinal;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.Timer;
import java.util.TimerTask;

/**
 This class is a service that runs over an increment of time and stores a location object
 */

public class ServiceLocation extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
    private Timer mTimer;
    GoogleApiClient mGoogleApiClient;
    LocationObject mLastLocation;
    ParseUser mCurrentUser = ParseUser.getCurrentUser();
    int mIntervalTime;


    /**
     Gets shared preferences and depending or not on whether the user has enabled location services, will start or log that its off
     */

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences sharedPref = getBaseContext().getSharedPreferences(Constants.DEV_PREFS, Context.MODE_PRIVATE);
        boolean locationToggle = sharedPref.getBoolean("LocationServiceEnabled", false);
        mIntervalTime = sharedPref.getInt("IntervalAmt", -1);
        if(locationToggle) {
            buildGoogleApiClient();
        }else{
            Log.d(Constants.SPROUT_TAG, "Location toggle is false, don't build");

        }
    }


    /**
    Creates a timer and that's dependant on the user's preferences and refreshes it
     */

    private void updateWithNewLocation() {

    //get location every 10 minutes and it

        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Location loc = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                newLocationObject(loc);
            }

        }, 0, mIntervalTime);
    }


    public void newLocationObject(Location loc) {
        if (loc == null) {
            mLastLocation = new LocationObject();
            mLastLocation.setLat(Constants.DEFAULT_LAT);
            mLastLocation.setLongi(Constants.DEFAULT_LONGI);
            mLastLocation.setUser(mCurrentUser);
        } else {
            ParseACL acl = new ParseACL(mCurrentUser);
            mLastLocation = new LocationObject();
            mLastLocation.setLat(loc.getLatitude());
            mLastLocation.setLongi(loc.getLongitude());
            mLastLocation.setUser(mCurrentUser);
            mLastLocation.setACL(acl);
            mLastLocation.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    Log.i(Constants.SPROUT_TAG, "Location registered");
                }
            });
        }
    }


    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTimer != null) {
            mTimer.cancel();
        }
    }

    @Override
    public boolean stopService(Intent name) {

        if (mTimer != null) {
            mTimer.cancel();
        }
        return super.stopService(name);
    }


    /**
     Creates API client
     */

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();


    }

    /**
     Various server methods,
     When connected, get the location straight away and start the service
     */

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(Constants.SPROUT_TAG, "API Client connected");
        updateWithNewLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(Constants.SPROUT_TAG, "Connection suspended " + i + " seconds");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(Constants.SPROUT_TAG, connectionResult.toString());
    }


}
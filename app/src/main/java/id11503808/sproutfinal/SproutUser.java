package id11503808.sproutfinal;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.ContactsContract;

import java.io.Serializable;
import java.util.List;

/**
 THIS CLASS IS NOT IN SCOPE BUT WILL BE IMPLEMENTED (mostly)
 At the moment this is the holder for the ParseUser on the profile pages but it currently is not used correctly to prevent lots of
 Queries
 */
public class SproutUser  {


    private String mObjId;
    private String mName;



    private String mProfileUrl;



    private int mSproutNumber;
    private List<String> mFlist;



    public String getmProfileUrl() {
        return mProfileUrl;
    }

    public void setmProfileUrl(String mProfileUrl) {
        this.mProfileUrl = mProfileUrl;
    }

    public String getmObjId() {
        return mObjId;
    }

    public void setmObjId(String mObjId) {
        this.mObjId = mObjId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public List<String> getmFlist() {
        return mFlist;
    }

    public String getmSproutNumber() {
        return "Sprouts: 1";
    }

    public void setmSproutNumber(int mSproutNumber) {
        this.mSproutNumber = mSproutNumber;
    }

    public void setmFlist(List<String> mFlist) {
        this.mFlist = mFlist;
    }









}

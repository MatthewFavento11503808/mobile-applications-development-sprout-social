package id11503808.sproutfinal;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.facebook.FacebookSdk;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;
import com.parse.ParseObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * This activity is the launcher activity for the app
 * Its a typical splash activity, waits a certain amount of time and then proceeds to LoginActivity
 * Also, some key registrations happen here for the entire app.
 * Facebbok, Parse, ParseFacebookUtils and various ParseObjects I use
 */

public class SplashActivity extends ActionBarActivity {

    ProgressBar mProgressBar;

    /**
     * On create, register and initialise various libraries
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        ParseObject.registerSubclass(SproutPost.class);
        ParseObject.registerSubclass(FriendRequest.class);
        ParseObject.registerSubclass(LocationObject.class);
        Parse.initialize(this, getString(R.string.parse_app_id), getString(R.string.parse_client_id));
        ParseFacebookUtils.initialize(this);
        setContentView(R.layout.activity_splash);
        waitSplash();
    }

    /**
     * This method is always run
     * Waits 'Constants.SPLASH_TIME' and starts 'LoginActivity'
     */

    private void waitSplash() {
        mProgressBar = (ProgressBar) findViewById(R.id.pgb_splash);
        mProgressBar.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                // close this activity
                finish();
            }
        }, Constants.SPLASH_TIME);
    }
}

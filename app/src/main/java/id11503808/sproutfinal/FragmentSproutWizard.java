package id11503808.sproutfinal;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 THIS CLASS IS NOT IN SCOPE BUT WILL BE IMPLEMENTED
 */
public class FragmentSproutWizard extends android.support.v4.app.DialogFragment {

    List<LocationObject> mLocationObjects;
    LocationObject mCurrentLocation;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sprout, container, false);
        return view;
    }


    public void setList(List<LocationObject> locations) {
        mLocationObjects = locations;
    }


    public void getLocationObject(int position) {
        mCurrentLocation = mLocationObjects.get(position);

    }
}


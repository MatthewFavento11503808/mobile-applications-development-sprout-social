package id11503808.sproutfinal;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.Date;

/**
        Locations Class integrated for easy uploading to Parse

 */

@ParseClassName("Locations")
public class LocationObject extends ParseObject {

    public double getLat() {
        return getDouble("latitude");
    }

    public void setLat(double value) {
        put("latitude", value);
    }

    public double getLongi() {
        return getDouble("longitude");
    }



    public void setLongi(double value) {
        put("longitude", value);
    }

    //THIS IS MUCH BETTER THAN USING OBJECT ID'S
    //IF IT WAS NOT AS CLOSE TO THE DUE DATE I WOULD GO BACK AND CORRECT THIS WITH FRIENDLIST
    public ParseUser getUser() {
        return getParseUser("user");
    }

    public void setUser(ParseUser value) {
        put("user", value);
    }

    public String getText() {
        return null;
    }


    public static ParseQuery<LocationObject> getQuery() {
        return ParseQuery.getQuery(LocationObject.class);
    }

}


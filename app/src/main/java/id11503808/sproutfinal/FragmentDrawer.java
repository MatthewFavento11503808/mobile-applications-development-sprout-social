package id11503808.sproutfinal;
 
/*
** FragmentNavigationDrawer object for use with support-v7 library
** using compatibility fragments and support actionbar
*/

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import id11503808.sproutfinal.NavAdapter;
import id11503808.sproutfinal.NavMenuItem;


/**
 * I did not design this Fragment, it was designed by Nathan Esquenazi
 * https://github.com/codepath/android_guides/wiki/Fragment-Navigation-Drawer
 * It's a DrawerLayout that has selectable options that opens a paired fragment
 * I did edit the code however
 */

public class FragmentDrawer extends DrawerLayout {

    private ActionBarDrawerToggle mDrawerToggle;
    private ListView mNavListView;
    private Toolbar mToolbar;
    private NavAdapter mNavAdapter;
    private ArrayList<NavMenuItem> mNavItems;
    private ArrayList<FragmentNavItem> mDrawerNavItems;
    private int mDrawerContainerRes;

    public FragmentDrawer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public FragmentDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FragmentDrawer(Context context) {
        super(context);
    }


    /**
     * Declares some class variables from arguments
     * Deals with actionbar
     */


    public void setupDrawerConfiguration(ListView drawerListView, Toolbar drawerToolbar, int drawerContainerRes) {
        mDrawerNavItems = new ArrayList<FragmentDrawer.FragmentNavItem>();
        mNavItems = new ArrayList<NavMenuItem>();
        mDrawerContainerRes = drawerContainerRes;
        mNavListView = drawerListView;
        mToolbar = drawerToolbar;
        mNavListView.setOnItemClickListener(new FragmentDrawerItemListener());
        mDrawerToggle = setupDrawerToggle();
        setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }


    /**
     * Uses arguments to create a new navigation element in the drawerlayout
     */

    public void addNavItem(String navTitle, String fragTitle, Class<? extends Fragment> fragmentClass, int imgRes) {
        mNavItems.add(new NavMenuItem(navTitle, imgRes));
        mNavAdapter = new NavAdapter(getActivity().getApplicationContext(), mNavItems);
        mDrawerNavItems.add(new FragmentNavItem(fragTitle, fragmentClass));
        mNavListView.setAdapter(mNavAdapter);
    }



    /**
     * Swaps fragments in the main content view
     */


    public void selectDrawerItem(int position) {
        FragmentNavItem navItem = mDrawerNavItems.get(position);
        Fragment fragment = null;
        try {
            fragment = navItem.getFragmentClass().newInstance();
            Bundle args = navItem.getFragmentArgs();
            if (args != null) {
                fragment.setArguments(args);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(mDrawerContainerRes, fragment).commit();
        // Highlight the selected item, update the title, and close the drawer
        mNavListView.setItemChecked(position, true);
        setTitle(navItem.getTitle());
        closeDrawer(mNavListView);
    }



    public ActionBarDrawerToggle getDrawerToggle() {
        return mDrawerToggle;
    }


    private FragmentActivity getActivity() {
        return (FragmentActivity) getContext();
    }

    private ActionBar getSupportActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    private void setTitle(CharSequence title) {
        getSupportActionBar().setTitle(title);
    }

    private class FragmentDrawerItemListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectDrawerItem(position);
        }
    }

    private class FragmentNavItem {
        private Class<? extends Fragment> fragmentClass;
        private String title;
        private Bundle fragmentArgs;
        private int imgRes;

        public FragmentNavItem(String title, Class<? extends Fragment> fragmentClass) {
            this(title, null, fragmentClass);
        }

        public FragmentNavItem(String title, Bundle args, Class<? extends Fragment> fragmentClass) {
            this.fragmentClass = fragmentClass;
            this.fragmentArgs = args;
            this.title = title;
        }

        public FragmentNavItem(String title, Bundle args, Class<? extends Fragment> fragmentClass, int imgRes) {
            this.fragmentClass = fragmentClass;
            this.fragmentArgs = args;
            this.title = title;
            this.imgRes = imgRes;
        }

        public Class<? extends Fragment> getFragmentClass() {

            return fragmentClass;
        }

        public String getTitle() {
            return title;
        }

        public Bundle getFragmentArgs() {
            return fragmentArgs;
        }

        public int getImgRes() {
            return imgRes;
        }
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(getActivity(), this, mToolbar, R.string.drawer_open, R.string.drawer_close);
    }

    public boolean isDrawerOpen() {
        return isDrawerOpen(mNavListView);
    }
}
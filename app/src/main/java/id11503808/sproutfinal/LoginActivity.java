package id11503808.sproutfinal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;

import com.facebook.login.widget.LoginButton;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;

import java.util.Arrays;
import java.util.List;


/**
 * This activity deals with logging into the App through Facebook (the only option)
 */

public class LoginActivity extends ActionBarActivity {

    private LoginButton mLoginButton;
    private ParseUser mCurrentUser;


    /**
     * onCreate
     * if mCurrentUser exists,
     * the user is logged in already (dealt with in the next activity) so go to the HomeActivity and don't even show this activity
     *
     * else display login button
     * Associates facebook LoginButton and listener
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mLoginButton = (LoginButton) findViewById(R.id.login_button);
        mCurrentUser = ParseUser.getCurrentUser();
        if ((mCurrentUser != null) && ParseFacebookUtils.isLinked(mCurrentUser)) showHomeActivity();
        mLoginButton.setOnClickListener(new FacebookListener());
    }

    /**
     * Opening homeActivity method
     */
    private void showHomeActivity() {
        Intent i = new Intent(this, HomeActivity.class);
        startActivity(i);
    }

    /**
     * Takes activity information and sends it to ParseFacebookUtils
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * The listener for the facebook button
     */

    private class FacebookListener implements View.OnClickListener {

        /**
         * When user clicks facebook button
         * ParseFacebookUtils made this easy with it's method "loginWithReadPermissionsInBackground"
         * Returns ParseUser as  currentUser
         * If theres no return, something went wrong
         * If the user isNew() (ParseUser method that checks Parse if user has first signed in from facebook)
         * If a user is returned, log em in
         */

        @Override
        public void onClick(View v) {
            List<String> permissions = Arrays.asList("public_profile", "email", "user_friends", "user_photos");
            ParseFacebookUtils.logInWithReadPermissionsInBackground(LoginActivity.this, permissions, new LogInCallback() {
                @Override
                public void done(ParseUser user, ParseException err) {
                    if (user == null) {
                        Log.d(Constants.SPROUT_TAG, "user cancelled");
                    } else if (user.isNew()) {
                        Log.d(Constants.SPROUT_TAG, "User signed up and logged in through Facebook!");
                        showHomeActivity();
                    } else {
                        Log.d(Constants.SPROUT_TAG, "User logged in through Facebook!");
                        showHomeActivity();
                    }
                }
            });
        }



    }




}
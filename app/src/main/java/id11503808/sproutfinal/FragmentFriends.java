package id11503808.sproutfinal;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * This class displays all mCurrentUser's friends
 */

public class FragmentFriends extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private ParseQueryAdapter<ParseUser> mFlistAdapter;
    private ParseUser mCurrentUser;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ParseQueryAdapter.QueryFactory<ParseUser> mFactory;
    private ListView mFriendsListView;
    private ArrayList<String> mFriendsList;

    /**
     * Works in a similar way to the posts adapter in the profile fragments
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friends, container, false);
        userFriendsQueryFactory();

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        mCurrentUser = ParseUser.getCurrentUser();
        mFriendsList = (ArrayList<String>)mCurrentUser.get("fList");
        mFlistAdapter = new ParseQueryAdapter<ParseUser>(getActivity(), mFactory) {

            @Override
            public View getItemView(ParseUser friend, View view, ViewGroup parent) {
                if (view == null) {
                    view = View.inflate(getActivity(), R.layout.fragment_friend_badge, null);
                }
                super.getItemView(friend, view, parent);
                Log.i(Constants.SPROUT_TAG, friend.getString("fb_name"));
                TextView postName = (TextView) view.findViewById(R.id.badge_name);
                postName.setText(friend.getString("fb_name"));
                return view;
            }
        };

        mFlistAdapter.notifyDataSetChanged();
        mFriendsListView = (ListView) view.findViewById(R.id.friends_lv);
        mFriendsListView.setAdapter(mFlistAdapter);
        mSwipeRefreshLayout.setOnRefreshListener(this);


        /**
         * Makes sure scrollview is at the top of the screen before refreshing
         */

        mFriendsListView.setOnScrollListener(new AbsListView.OnScrollListener() {


            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (mFriendsListView == null || mFriendsListView.getChildCount() == 0) ?
                                0 : mFriendsListView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 &&
                        topRowVerticalPosition >= 0);
            }
        });


        /**
         * Clicking on a list item will get the user at that position
         */

        mFriendsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ParseUser parseUser = mFlistAdapter.getItem(position);
                switchToFriendFragment(parseUser.getObjectId());
            }
        });

        return view;
    }



    /**
     * Refreshes the adapter
     */

    @Override
    public void onRefresh() {
        Toast.makeText(getActivity(), "Refreshing", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mFlistAdapter.loadObjects();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 2000);

        for (String friend : mFriendsList) {
        }
    }

    /**
     * Runs homeactivity method that switches the fragment view
     */

    public void switchToFriendFragment(String friendId){
        Bundle args = new Bundle();
        args.putString("friendId", friendId);
        HomeActivity hmActivity = (HomeActivity)getActivity();
        hmActivity.switchToFriendProfile(args);
    }


    /**
     * mFactory that gets each user in the user's friendslist and sets to adapter
     */

    private void userFriendsQueryFactory(){
        mFactory = new ParseQueryAdapter.QueryFactory<ParseUser>() {
            public ParseQuery<ParseUser> create() {
                ParseQuery<ParseUser> query = ParseQuery.getQuery(ParseUser.class);
                query.whereContainedIn("objectId", mFriendsList);
                query.setLimit(12);
                //query.orderByDescending("createdAt");
                return query;
            }
        };
    }

}
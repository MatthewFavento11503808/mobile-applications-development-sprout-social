package id11503808.sproutfinal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Switch;

import com.parse.ParseUser;

/**
 *Fragment where user can edit settings pertaining to the app. Currently they can only edit one option - Location Services
 */
public class FragmentSettings extends Fragment {

    Button mLogoutBtn;
    Button mSaveBtn;
    Spinner mIntervalSpinner;
    SharedPreferences.Editor editor;
    HomeActivity hmAc;
    Switch mLocationSwitch;
    boolean mLocationToggle = true;
    SharedPreferences mSharedPrefs;
    String mSpinnerText;


    /**
     * Inflates the view and fills the settings view with what the user has already picked or default views
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_settings, container, false);
        Context context = getActivity();
        hmAc = (HomeActivity)getActivity();
        mSharedPrefs = context.getSharedPreferences(Constants.DEV_PREFS, Context.MODE_PRIVATE);
        mLogoutBtn = (Button)view.findViewById(R.id.btn_logout);
        mSaveBtn = (Button)view.findViewById(R.id.btn_save);
        mIntervalSpinner = (Spinner)view.findViewById(R.id.interval_spinner);
        mLocationSwitch = (Switch) view.findViewById(R.id.locations_switch);
        editor =  mSharedPrefs.edit();

        mLogoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParseUser.logOut();
                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
            }
        });


        /**
         * Saves the user input to shared preferences
         */

        mSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String spinnerText = mIntervalSpinner.getSelectedItem().toString();
                mLocationToggle = mLocationSwitch.isChecked();
                int mSpinnerAmt = 900000;
                switch(spinnerText){
                    case "15 Minutes":
                        mSpinnerAmt = 900000;
                         break;
                    case "45 Minutes":
                        mSpinnerAmt = 2700000;
                        break;
                    case "60 Minutes":
                        mSpinnerAmt = 3600000;
                        break;
                    case "120 Minutes":
                        mSpinnerAmt = 7200000;
                        break;
                }
                editor.putInt("IntervalAmt", mSpinnerAmt);
                editor.putBoolean("LocationServiceEnabled", mLocationToggle);

                editor.commit();

                hmAc.restartService();
            }
        });
        fillSettingsDefault();


        return view;
    }



    public void fillSettingsDefault(){
        mLocationToggle = mSharedPrefs.getBoolean("LocationServiceEnabled", false);
        mLocationSwitch.setChecked(mLocationToggle);
        switch(mSharedPrefs.getInt("IntervalAmt", -1)){
            case 900000:
                mIntervalSpinner.setSelection(0);
                break;
            case 2700000:
                mIntervalSpinner.setSelection(1);
                break;
            case 3600000:
                mIntervalSpinner.setSelection(2);
                break;
            case 7200000:
                mIntervalSpinner.setSelection(3);
                break;
        }
        }

        }




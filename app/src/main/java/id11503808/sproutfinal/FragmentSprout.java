package id11503808.sproutfinal;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;

/**
 * Created by matt on 1/06/2015.
 */
public class FragmentSprout extends DialogFragment {

    LocationObject mLocation;
    View mView;
    HomeActivity hmAc;



    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_sprout, container, false);
        hmAc = (HomeActivity)getActivity();
        Button openLocationChooser = (Button)mView.findViewById(R.id.btn_choose);
        Button deleteLocation = (Button)mView.findViewById(R.id.btn_delete);
        Button decideLater = (Button)mView.findViewById(R.id.btn_decide);

        openLocationChooser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mLocation != null) {
                    Spinner doSpinner = (Spinner) mView.findViewById(R.id.do_spinner);
                    String spinnerText = doSpinner.getSelectedItem().toString();
                    hmAc.openLocationChooser(mLocation, spinnerText);
                }

            }
        });




        deleteLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLocation != null) {
                hmAc.promptDeleteDialog(mLocation);
                }
            }
        });

        decideLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismissDialogu();

            }
        });




        return mView;
    }

    public void setLocation(LocationObject location){
        mLocation = location;
    }


    public void dismissDialogu(){
        getDialog().dismiss();
    }


}


package id11503808.sproutfinal;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This is the class that is displayed when user views their own profile
 * The class 'FragmentFriendProfile' is very similar to this one, but deals with a user other than the logged in user
 * I could theoretically implement this class to run both the user and the viewed user
 */

public class FragmentProfile extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    ParseUser mCurrentUser;

    TextView mBadgeName;
    //TextView mBadgeSproutNumber;
    ImageView mBadgePicture;
    private ListView mPostsLv;
    SwipeRefreshLayout mSwipeRefreshLayout;

    ParseQueryAdapter mPostAdapter;
    private ParseQueryAdapter.QueryFactory<SproutPost> mFactory;
    SproutUser mSproutUser;


    /**
     * Starts the post adapter and associates all required fields
     * Creates an adapter from query method (mFactory is used to run the queryadapter)
     * Various methods for the swipeRefresh
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        setHasOptionsMenu(true);
        mCurrentUser = ParseUser.getCurrentUser();
        mPostsLv = (ListView) view.findViewById(R.id.posts_lv);
        mBadgeName = (TextView) view.findViewById(R.id.badge_name);
       // mBadgeSproutNumber = (TextView) view.findViewById(R.id.badge_sprouts);
        mBadgePicture = (ImageView) view.findViewById(R.id.badge_picture);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        userPostsQueryFactory();

        mPostAdapter = new ParseQueryAdapter<SproutPost>(getActivity(), mFactory) {
            @Override
            public View getItemView(SproutPost sprout, View view, ViewGroup parent) {
                if (view == null) view = View.inflate(getActivity(), R.layout.posts_item, null);
                TextView postName = (TextView) view.findViewById(R.id.post_name);
                TextView postText = (TextView) view.findViewById(R.id.post_text);
                postName.setText(sprout.getName());
                postText.setText(sprout.getText());
                return view;
            }
        };
        mPostAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mPostsLv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (mPostsLv == null || mPostsLv.getChildCount() == 0) ?
                                0 : mPostsLv.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 &&
                        topRowVerticalPosition >= 0);
            }
        });

        mPostAdapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener() {
            @Override
            public void onLoading() {
                //set progress bar to visible
            }

            @Override
            public void onLoaded(List list, Exception e) {

            }
        });
        fillExecution();

        return view;
    }


    /**
     * This method uses to recursion to assign a 'SproutUser' object to the profile
     * This is so that in order to get information about the user, a query only needs to be run once (future scope)
     *
     * If there is no existing 'mSproutUser' this method will create a new one and assign it variables
     * pertaining to the 'mCurrentUser' ParseUser object (the profile the user is viewing)
     */

    private void fillExecution() {
        if (checkIfExistingProfile()) {
            fillProfileWithData();
            new GetProfilePicFromUrl(mSproutUser.getmProfileUrl()).execute();
        } else {
            makeNewSproutUser(mCurrentUser.getObjectId());
            fillExecution();
        }


    }


    private boolean checkIfExistingProfile() {
        if (mSproutUser != null) {
            //User exists
            //check if the same user as passed objectId
            return true;
        } else {
            return false;
        }
    }


    /**
     * Sets UI elements from mSproutUser object
     */


    private void fillProfileWithData() {
        mBadgeName.setText(mSproutUser.getmName());
        //mBadgeSproutNumber.setText(mSproutUser.getmSproutNumber());
        mPostsLv.setAdapter(mPostAdapter);
        mPostAdapter.notifyDataSetChanged();
    }


    /**
     *  Method that creates a new SproutUser object assigned to the class with the mCurrentUser's most update information
     *  from Facebook.
     */


    private void makeNewSproutUser(String id) {
        mSproutUser = new SproutUser();
        mSproutUser.setmObjId(id);
        mSproutUser.setmFlist((ArrayList<String>) mCurrentUser.get("fList"));
        mSproutUser.setmName(mCurrentUser.getString("fb_name"));
        mSproutUser.setmProfileUrl(mCurrentUser.getString("picture_url"));

    }



    /**
     * Query to get mCurrentUsers posts
     */


    private void userPostsQueryFactory() {
        mFactory = new ParseQueryAdapter.QueryFactory<SproutPost>() {
            public ParseQuery<SproutPost> create() {
                ParseQuery<SproutPost> query = SproutPost.getQuery();
                query.include("user");
                query.whereEqualTo("user", mCurrentUser);
                query.setLimit(12);
                query.orderByDescending("createdAt");
                return query;
            }
        };
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.dev_action_post: //DEV SETTING
                createPost();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * Creates a post with information from mCurrentUser and a random bar name
     * ACL to allow all users to read it
     * Saves the post to Parse
     */

    private void createPost() {
        SproutPost sprout = new SproutPost();
        sprout.setUser(mCurrentUser);
        sprout.setName(mCurrentUser.getString("fb_name"));
        sprout.setVenue(randomBarName());
        sprout.setAction("visited");
        sprout.setTime(mCurrentUser.getCreatedAt());

        //read only access for others for the post
        ParseACL acl = new ParseACL();
        acl.setPublicReadAccess(true);
        sprout.setACL(acl);
        sprout.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
            }
        });

    }

    /**
     * Method to generate a bar name from 3 numbers
     */

    private String randomBarName() {
        Random rand = new Random();
        int randomNum = rand.nextInt((999 - 100) + 1) + 100;
        return "Bar " + randomNum;
    }




    @Override
    public void onRefresh() {
        Toast.makeText(getActivity(), "Refreshing...", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mPostAdapter.loadObjects();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 2000);
    }



    /**
     * AsyncTask that fetches picture from Parse a profile picture url for that User
     * Sets the image to the facebook profile pic
     */

    public class GetProfilePicFromUrl extends AsyncTask<Void, Bitmap, Void> {

        String mImgUrl;

        public GetProfilePicFromUrl(String imgUrl) {
            super();
            this.mImgUrl = imgUrl;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(mImgUrl).getContent());
                publishProgress(bitmap);
            } catch (IOException e) {
                Log.i(Constants.SPROUT_TAG, e.toString());
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Bitmap... profPic) {
            super.onProgressUpdate(profPic);
            mBadgePicture.setImageBitmap(profPic[0]);

        }
    }
}





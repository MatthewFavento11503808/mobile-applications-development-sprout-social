package id11503808.sproutfinal;

/**
 * Created by matt on 20/05/2015.
 */
public class Constants {
    public final static int SPLASH_TIME = 2000;
    public final static String SPROUT_TAG = "SPROUT_TAG";
    public final static String FAV_ID = "JmRw182Msm";
    public final static String MATT_ID = "4twtTY9agg";
    public final static double DEFAULT_LONGI = 0.0;
    public final static double DEFAULT_LAT = 0.0;
    public final static String DELETE_LOCATION_MESSAGE = "Are you sure you want to remove this location? You can't get it back";
    public final static String SPROUT_LOCATION_MESSAGE = "Are you sure you want to sprout this location? It will become public";
    public static final String DEV_PREFS = "DevPrefsFile";


}

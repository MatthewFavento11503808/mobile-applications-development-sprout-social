package id11503808.sproutfinal;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 THIS CLASS IS NOT IN SCOPE BUT WILL BE IMPLEMENTED
 */


@ParseClassName("FriendRequests")
public class FriendRequest extends ParseObject {

    public String getSender() {
        return getString("sender");
    }

    public void setSender(String sender) {
        put("sender", sender);
    }

    public String getReceiver() {
        return getString("sender");
    }

    public void setReceiver(String receiver) {
        put("receiver", receiver);
    }


    public static ParseQuery<FriendRequest> getQuery() {
        return ParseQuery.getQuery(FriendRequest.class);
    }
}

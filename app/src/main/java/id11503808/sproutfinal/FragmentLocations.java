package id11503808.sproutfinal;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.List;
import java.util.Random;


 /**
 * This class is the backend for a listView that contains all the registered locations of the current user
 */

 public class FragmentLocations extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private ParseQueryAdapter.QueryFactory<LocationObject> mFactory;
    ParseUser mCurrentUser = ParseUser.getCurrentUser();
    ListView mPostsLv;
    ParseQueryAdapter mPostAdapter;
    SwipeRefreshLayout mSwipeRefreshLayout;

     /**
      * Adapter code similar to profile
      */



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_locations, container, false);
        mPostsLv = (ListView) view.findViewById(R.id.locations_lv);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        setHasOptionsMenu(true);

        userPostsQueryFactory();
        mPostAdapter = new ParseQueryAdapter<LocationObject>(getActivity(), mFactory) {
            @Override
            public View getItemView(LocationObject location, View view, ViewGroup parent) {
                if (view == null) view = View.inflate(getActivity(), R.layout.location_item, null);
                TextView locationText = (TextView) view.findViewById(R.id.location_text);
                locationText.setText(getLocationText(location));
                return view;

            }
        };

        mPostAdapter.notifyDataSetChanged();
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mPostsLv.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (mPostsLv == null || mPostsLv.getChildCount() == 0) ?
                                0 : mPostsLv.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(firstVisibleItem == 0 &&
                        topRowVerticalPosition >= 0);
            }
        });

        mPostAdapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener() {
            @Override
            public void onLoading() {
                //set progress bar to visible
            }

            @Override
            public void onLoaded(List list, Exception e) {

            }
        });

        mPostsLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LocationObject location = (LocationObject)mPostAdapter.getItem(position);
                onLocationClick(location);

            }
        });
        mPostsLv.setAdapter(mPostAdapter);

        return view;
    }


     /**
      * These methods run  HomeActivity methods, see activity for a description of why they're there
      */

    private void onLocationClick(LocationObject location){
        HomeActivity hmAc = (HomeActivity) getActivity();
        hmAc.openSproutFragment(location);
    }


     /**
      * Returns the text of the location
      */

    private String getLocationText(LocationObject location){
        String locationText = "Date: " + location.getCreatedAt();
        return locationText;
    }

    private void userPostsQueryFactory() {
        mFactory = new ParseQueryAdapter.QueryFactory<LocationObject>() {
            public ParseQuery<LocationObject> create() {
                ParseQuery<LocationObject> query = LocationObject.getQuery();
                query.include("user");
                query.whereEqualTo("user", mCurrentUser);
                query.setLimit(12);
                query.orderByDescending("createdAt");
                return query;
            }
        };
    }

    @Override
    public void onRefresh() {
        Toast.makeText(getActivity(), "Refreshing", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mPostAdapter.loadObjects();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 2000);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_locations, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.dev_action_random_location:{
                addRandomLocation();
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }


     /**
      * Dev code to add a random location generated from 'randomObject' method and save it to Parse
      */

    private void addRandomLocation(){
        LocationObject randomLocation = randomObject();
        randomLocation.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Log.i(Constants.SPROUT_TAG, "Location registered");
            }
        });
    }


     /**
      * Will return 1 of 4 random locations
      */

    private LocationObject randomObject(){
        Random rand = new Random();
        int randomNum = rand.nextInt((3 - 0) + 1) + 0;
        ParseACL acl = new ParseACL(mCurrentUser);
        LocationObject randomLocation = new LocationObject();
        randomLocation.setACL(acl);
        randomLocation.setUser(mCurrentUser);
        double latitude = -33.895791;
        double longitude = 151.179826;

        switch (randomNum){
            case 0:
                //marketplace
                latitude = -33.884776;
                longitude = 151.149053;
                break;
            case 1:
                //marys
                latitude = -33.895791;
                longitude = 151.179826;
                break;

            case 2:
                //broadway gym
                latitude = -33.884352;
                longitude = 151.196466;
                break;

            case 3:
                //world bar
                latitude = -33.874857;
                longitude = 151.223980;
                break;

        }
        randomLocation.setLat(latitude);
        randomLocation.setLongi(longitude);
        return randomLocation;
    }


}
